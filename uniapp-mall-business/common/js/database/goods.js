import store from '@/store/index.js'
// 默认选择驿站
let userInfo = store.state.userInfo;
let obj = {
	required: true,
	message: '必填项，请输入',
	trigger: 'blur' ,
};
let goodsRule = {
	productName: [obj],businessName: [],virtualAmount: [obj],productPlace: [obj],typeName:[obj]
};
let goodsAttribute = {
	businessId : userInfo.id,businessAvatar: userInfo.avatarUrl,productName:'' , businessName:userInfo.busiName , virtualAmount:'' , productPlace: '',typeName:'',buyLimit:0,warningValue:20,productSort:99999,integral:1,shareOne:8,shareTwo:2
};

module.exports = {
	goodsRule,goodsAttribute
};