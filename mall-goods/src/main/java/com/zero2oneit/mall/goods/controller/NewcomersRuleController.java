package com.zero2oneit.mall.goods.controller;

import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.goods.service.NewcomersRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-06-02
 */
@RestController
@RequestMapping("/admin/newcomers")
public class NewcomersRuleController {

    @Autowired
    private NewcomersRuleService newcomersRuleService;

    /**
     * 查询新人专享规则列表
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody NewcomersRuleQueryObject qo){
        return newcomersRuleService.pageList(qo);
    }

    /**
     * 添加或编辑新人专享规则信息
     * @param newcomersRule
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody NewcomersRule newcomersRule){
        newcomersRule.setOperatorTime(new Date());
        newcomersRuleService.saveOrUpdate(newcomersRule);
        return R.ok();
    }

    /**
     * 删除新人专享规则信息
     * @param ids
     * @return
     */
    @PostMapping("/delByIds")
    public R delByIds(@RequestBody String ids){
        return newcomersRuleService.removeByIds(Arrays.asList(ids.split(","))) == true ? R.ok("删除成功") : R.fail("删除失败");
    }

    /**
     * 更改新人专享规则信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("id") String id, @RequestParam("status") Integer status){
        return newcomersRuleService.status(id, status);
    }

}
