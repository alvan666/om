package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.bean.member.PrizeWin;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.PrizeWinQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zero2oneit.mall.member.service.PrizeWinService;

import java.util.Arrays;
import java.util.Date;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@RestController
@RequestMapping("/admin/member/prizeWin")
public class PrizeWinController {

    @Autowired
    private PrizeWinService prizeWinService;

    /**
     * 查询中奖记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/winList")
    public BoostrapDataGrid winList(@RequestBody PrizeWinQueryObject qo){
        return prizeWinService.pageList(qo);
    }

    /**
     * 更改中奖记录状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/IssueStatus")
    public R IssueStatus(@RequestParam("id") String id, @RequestParam("status") Integer status){

        PrizeWin prizeWin = new PrizeWin();
        prizeWin.setId(Long.valueOf(id));
        prizeWin.setGrantTime(new Date());
        if (status==1){
            prizeWin.setStatusId(0);
        }else if (status==0){
            prizeWin.setStatusId(1);
        }
        return prizeWinService.updateById(prizeWin) == true ? R.ok("更改状态成功"):R.fail("更改状态失败");
    }

}
