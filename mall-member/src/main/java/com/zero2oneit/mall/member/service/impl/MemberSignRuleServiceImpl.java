package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.mapper.MemberSignRuleMapper;
import com.zero2oneit.mall.member.service.MemberSignRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@Service
public class MemberSignRuleServiceImpl extends ServiceImpl<MemberSignRuleMapper, MemberSignRule> implements MemberSignRuleService {

    @Autowired
    private MemberSignRuleMapper memberSignRuleMapper;

    @Override
    public BoostrapDataGrid ruleList(MemberSignRuleQueryObject qo) {
        int total = memberSignRuleMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : memberSignRuleMapper.selectAll(qo));
    }

}